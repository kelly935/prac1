import os

output_location = "Bin/game"

cpp_file_list = [
    "Source/Human.cpp",
    "Source/Computer.cpp",
    "Source/Referee.cpp",
    "Source/main.cpp"
]

input_files = ' '.join(cpp_file_list)

os.system("g++ -std=c++11 -O2 -Wall -o " + output_location + " " + input_files)