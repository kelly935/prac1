#ifndef _REFEREE_H_
#define _REFEREE_H_
#include "Human.h"
#include "Computer.h"

class Referee
{
    public:
        Referee(); 
        //Referee will check any combination of human and computer player results
        char refGame(HumanPlayer player1, ComputerPlayer player2);
    private:

};

#endif