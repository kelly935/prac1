#include <iostream>
#include "Referee.h"

Referee::Referee()
{

}

char Referee::refGame(HumanPlayer player1, ComputerPlayer player2)
{
    char computer_move = player2.makeMove();
    char player_move = player1.makeMove();

    switch (computer_move)
    {
        //If the computer player chooses Rock
        case 'R':

            //Check the corresponding human player move
            switch (player_move)
            {
                case 'R':
                    return 'T';
                case 'P':
                    return 'W';
                case 'S':
                    return 'L';
            }
        
        //If the computer player chooses Paper
        case 'P':

            //Check the corresponding human player move
            switch (player_move)
            {
                case 'R':
                    return 'L';
                case 'P':
                    return 'T';
                case 'S':
                    return 'W';             
            }

        //If the computer player chooses Scissors
        case 'S':

            //Check the corresponding human player move
            switch (player_move)
            {
                case 'R':
                    return 'W';
                case 'P':
                    return 'L';
                case 'S':
                    return 'T';             
            }
    }

    //If we get garbage then return lose by default
    return 'L';
}