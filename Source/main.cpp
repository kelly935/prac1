#include <iostream>
#include <string>
#include "Human.h"
#include "Computer.h"
#include "Referee.h"

int main()
{
    HumanPlayer humanplayer1;
    ComputerPlayer pcplayer2;
    Referee referee;

    int numGames = 4;
    std::string results = "";

    for(int i = 0; i < numGames; i++)
    {
        results += referee.refGame(humanplayer1, pcplayer2);

        //If not the last result add in a space
        if(i != numGames - 1)
        {
            results += ' ';
        }
    }

    std::cout << results << std::endl;
    
}